export const navLinks = [
  // {
  //   label: 'Projects',
  //   pathname: '/#project-1',
  // },
  {
    label: 'Details',
    pathname: '/#details',
  },
  {
    label: 'Contact',
    pathname: '/contact',
  },
];

export const socialLinks = [
  // {
  //   label: 'Twitter',
  //   url: 'https://twitter.com/dharmendraMW',
  //   icon: 'twitter',
  // },
  {
    label: 'DevCommunity',
    url: 'https://dev.to/dharmendraboddeda',
    icon: 'dev',
  },
  {
    label: 'Figma',
    url: 'https://www.figma.com/files/team/1213875332472489437',
    icon: 'figma',
  },
  {
    label: 'GitLab',
    url: 'https://gitlab.com/dboddeda',
    icon: 'gitlab',
  },
  {
    label: 'GitHub',
    url: 'https://github.com/dharmendraboddeda',
    icon: 'github',
  },
];
